---
title: "Loading Confirmation"
metaTitle: "EDI Loading Confirmation"
metaDescription: "This is the meta description for this page"
---
# EDI Loading Confirmation

Loading confirmations are uploaded to the EDI FTP server in the `loading_confirmations` subfolder.
The confirmation should be exactly 1 file.

```
us-warehouse-01/
  loading_confirmations/
    8add6055461843aa89d707c689f8a69c_v1.json
```

## EDI Message

### Filename structure
`ORDERID_YYYYMMDDHHMMSS.json`, example: `8add6055461843aa89d707c689f8a69c_20200727120159.json`

### EDI Delivery Times
Files should be uploaded as soon as packages are handed over to the courier.

### Example
```json
{
    "type": "loading-confirmation",
    "order_id": "a5fdd5b0bdf6425980598a44fb5850cd",
    "loading_date": "2020-07-29",
    "warehouse_id": "at-gleisdorf-01",
    "packages": [
        {
            "package_id": "001",
            "tracking_nr": "ZCK8GBOE",
            "stock": [
                {
                    "sku": "WAT-HAL-STL-05",
                    "quantity": 1,
                    "serial_nr": "21102019084658LO0038"
                },
                {
                    "sku": "PAK-MBX-HAL-WAT-STL-05",
                    "quantity": 1
                }
            ]
        },
        {
            "package_id": "002",
            "tracking_nr": "ZCK8GBOF",
            "stock": [
                {
                    "sku": "WAT-HAL-STL-05",
                    "quantity": 1,
                    "serial_nr": "21102019084650LO0037"
                },
                {
                    "sku": "PAK-MBX-HAL-WAT-STL-05",
                    "quantity": 1
                }
            ]
        },
        {
            "package_id": "003",
            "tracking_nr": "ZCK8GBOG",
            "stock": [
                {
                    "sku": "DPR-OAS-ONX-CTP-BKS-EU",
                    "quantity": 1
                }
            ]
        }
    ]
}
```


### Elements
#### Root
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `type` | `string` | | Yes | Always `loading-confirmation` |
| `order_id` | `string` | 32 | Yes | The unique ID for this order |
| `loading_date` | `string` | 10 | Yes | When it was released to courier (format `YYYY-MM-DD`) |
| `warehouse_id` | `string` | | Yes | The warehouse where this message originates from (e.g. `us-newjersey-01`) |
| `packages` | `array<map>` | Min 1 | Yes | See `packages` |

#### packages
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `package_id` | `string` | 3 | Yes | The unique identifier within this order for this package (e.g. `001`, `012`) |
| `tracking_nr` | `string` | | Yes | Courier tracking number |
| `stock` | `array<map>` | Min 1 | Yes | See `packages / stock` |

#### packages / stock
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `sku` | `string` | | Yes | The SKU number of the item (e.g. `HAL-WAT-STL-05`) |
| `quantity` | `integer` | | Yes | Amount |
| `serial_nr` | `string` | | No* | Obligatory when handling water (must be scanned from package) | 