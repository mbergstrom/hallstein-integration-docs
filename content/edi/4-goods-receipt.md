---
title: "Goods Receipt"
metaTitle: "EDI Goods Receipt"
metaDescription: "This is the meta description for this page"
---
# EDI Goods Receipt

A `Goods Receipt` EDI message is sent by the warehouse to confirm that goods have been received and have been added to stock.

```
us-warehouse-01/
  goods_receipts/
    c13c3453bcb149f98ebeb3d71d201520.json
```

The EDI message should be uploaded to Hallstein after successful unload of the shipment.

## EDI Message

### Filename structure for order instructions
`ORDERID.json`, example: `c13c3453bcb149f98ebeb3d71d201520.json`


## Example 
```json
{
    "type": "goods-receipt",
    "order_id": "c13c3453bcb149f98ebeb3d71d201520",
    "loading_date": "2020-03-06",
    "warehouse_id": "us-newjersey-01",
    "packages": [
        {
            "pallet_id": "391200900110000129",
        },
        {
            "pallet_id": "391200900100003956",
        }
    ]
}
```

### Elements
#### Root
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `type` | `string` | | Yes | Always `goods-receipt` |
| `order_id` | `string` | 32 | Yes | The id of the `shipment-advice` |
| `warehouse_id` | `string` | | Yes | The warehouse that received the goods (e.g. `us-newjersey-01`) |
| `loading_date` | `string` | 10 | Yes | The date it was unloaded (format `YYYY-MM-DD`) |
| `packages` | `array<map>` | Min 1 | Yes | See `packages` |

#### packages
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `pallet_id` | `string` | 18 | Yes | The `id` of the pallet |
