---
title: "Shipment Advice"
metaTitle: "EDI Shipment Advice"
metaDescription: "This is the meta description for this page"
---
# EDI Shipment Advice

A `Shipment Advice` EDI message is sent to inform the warehouse that they can be expecting an inbound shipment. **Please note** that currently only water pallets are included in the shipment advice. This will be extended shortly. The format for non-water pallets will be the same or similar.

The important information to store are the pallet ids and serial numbers for the water. The serial numbers are later used in `Loading Confirmation` EDI messages for shipments to customers.

```
us-warehouse-01/
  shipment_advices/
    c13c3453bcb149f98ebeb3d71d201520.json
```

## EDI Message

### Filename structure for order instructions
`ORDERID.json`, example: `c13c3453bcb149f98ebeb3d71d201520.json`


## Example 
This example has been shortened for ease of reading purposes.

```json
{
  "warehouse_id": "at-gleisdorf-01",
  "type": "shipment-advice",
  "order_id": "c13c3453bcb149f98ebeb3d71d201520",
  "order_date": "2020-07-20",
  "expected_arrival_date": "2020-07-21",  
  "shipment": {
    "type": "truck",
    "license_nr": "",
    "from_warehouse_id": "at-factory-01"
  },
  "ship_to": {
    "state_province": "",
    "residential": false,
    "postcode": "8200",
    "phone_nr": "+43 1111111111",
    "name": "Warehouse 1",
    "email": "warehouse1@example.com",
    "country_code": "AT",
    "city": "Gleisdorf",
    "att_name": "",
    "address_line2": "",
    "address_line1": "Mühlgasse 146"
  },
  "ship_from": {
    "state_province": "Oberoesterreich",
    "residential": false,
    "postcode": "4831",
    "phone_nr": "+43 6648903378",
    "name": "Warehouse 2",
    "email": "warehouse2@example.com",
    "country_code": "AT",
    "city": "Obertraun",
    "att_name": "",
    "address_line2": "",
    "address_line1": "Obertraun 311"
  },
  "pallets": [
    {
      "weight": {
        "value": 590.825,
        "unit": "KG"
      },
      "pallet_id": "391200900110000129",
      "inventory": [
        {
          "weight": {
            "value": 12.685,
            "unit": "KG"
          },
          "sku": "WAT-HAL-STL-8PC-750",
          "serial_nrs": [
            "05052020090725BA1089",
            "05052020090640BA1088",
            "05052020090504BA1087",
            "05052020085438BA1086",
            "05052020085300BA1085",
            "05052020085139BA1084",
            "05052020085034BA1083",
            "05052020085022BA1082",
            "05052020085011BA1081",
            "05052020084520BA1080",
            "05052020084421BA1079",
            "05052020084136BA1078",
            "05052020084026BA1077",
            "05052020083934BA1076",
            "05052020083758BA1075",
            "05052020083449BA1074",
            "05052020083321BA1073",
            "05052020083150BA1072",
            "05052020083027BA1071",
            "05052020082935BA1070",
            "05052020082640BA1069",
            "05052020082525BA1068",
            "05052020082356BA1067",
            "05052020082317BA1066",
            "05052020082129BA1065",
            "05052020081918BA1064",
            "05052020120006BA1063",
            "05052020115919BA1062",
            "05052020115848BA1061",
            "05052020115353BA1060",
            "05052020115301BA1059",
            "05052020115134BA1058",
            "05052020115010BA1057",
            "05052020114945BA1056",
            "05052020114907BA1055",
            "05052020114831BA1054",
            "05052020114759BA1053",
            "05052020114229BA1052",
            "05052020114148BA1051",
            "05052020114002BA1050",
            "05052020113929BA1049",
            "05052020113905BA1048",
            "05052020113547BA1047",
            "05052020113506BA1046",
            "05052020113111BA1045"
          ],
          "quantity": 45,
          "dimensions": {
            "width": 33.0,
            "unit": "CM",
            "height": 35.0,
            "depth": 33.0
          },
          "description": "Hallstein Water 750ml 8-pack"
        }
      ],
      "dimensions": {
        "width": 110,
        "unit": "CM",
        "height": 244,
        "depth": 110
      },
      "description": "Pallet for Hallstein water bottles"
    },
    {
      "weight": {
        "value": 1236.0,
        "unit": "KG"
      },
      "pallet_id": "391200900100003956",
      "inventory": [
        {
          "weight": {
            "value": 19.0,
            "unit": "KG"
          },
          "sku": "WAT-HAL-STL-05",
          "serial_nrs": [
            "08062020115535PO0269",
            "08062020115512PO0268",
            "08062020115502PO0267",
            "08062020115454PO0266",
            "08062020115444PO0265",
            "08062020115437PO0264",
            "08062020115426PO0263",
            "08062020115419PO0262",
            "08062020115408PO0261",
            "08062020115401PO0260",
            "08062020115050PO0259",
            "08062020115042PO0258",
            "08062020115006PO0257",
            "08062020114958PO0256",
            "08062020114948PO0255",
            "08062020114940PO0254",
            "08062020114930PO0253",
            "08062020114922PO0252",
            "08062020114912PO0251",
            "08062020114904PO0250",
            "08062020114652PO0249",
            "08062020114629PO0248",
            "08062020114557PO0247",
            "08062020114534PO0246",
            "08062020114523PO0245",
            "08062020114516PO0244",
            "08062020114505PO0243",
            "08062020114458PO0242",
            "08062020114430PO0239",
            "08062020114422PO0238",
            "08062020114412PO0237",
            "08062020114405PO0236",
            "08062020114354PO0235",
            "08062020114347PO0234",
            "08062020114336PO0233",
            "08062020114329PO0232",
            "08062020114319PO0231",
            "08062020114311PO0230",
            "08062020114302PO0229",
            "08062020113838PO0224",
            "08062020113830PO0223",
            "08062020113811PO0222",
            "08062020113804PO0221",
            "08062020113744PO0220",
            "08062020113737PO0219",
            "08062020113718PO0218",
            "08062020113710PO0217",
            "08062020113332PO0216",
            "08062020113325PO0215",
            "08062020113219PO0214",
            "08062020113211PO0213",
            "08062020113049PO0212",
            "08062020113041PO0211",
            "08062020112326PO0207",
            "08062020112351PO0210",
            "08062020112344PO0209",
            "08062020112333PO0208",
            "08062020112316PO0206",
            "08062020112308PO0205",
            "08062020112258PO0204",
            "08062020112250PO0203",
            "08062020112240PO0202",
            "08062020112233PO0201",
            "08062020112222PO0200"
          ],
          "quantity": 64,
          "dimensions": {
            "width": 25.0,
            "unit": "CM",
            "height": 45.0,
            "depth": 25.0
          },
          "description": "Hallstein Water 5 Gallon Bottle"
        }
      ],
      "dimensions": {
        "width": 110,
        "unit": "CM",
        "height": 244,
        "depth": 110
      },
      "description": "Pallet for Hallstein water bottles"
    }
  ]
}
```

### Elements
#### Root
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `type` | `string` | | Yes | Always `shipment-advice` |
| `warehouse_id` | `string` | | Yes | The warehouse this message is intended for (e.g. `us-newjersey-01`) |
| `order_id` | `string` | 32 | Yes | The unique ID for this order |
| `order_date` | `string` | 10 | Yes | The date it was ordered (format `YYYY-MM-DD`) |
| `expected_arrival_date` | `string` | 10 | Yes | The date shipment should arrive (format `YYYY-MM-DD`) |
| `shipment` | `map` | | Yes | See `shipment` |
| `ship_to` | `map` | | Yes | See `ship_to` |
| `ship_from` | `map` | | Yes | See `ship_from` |
| `pallets` | `array<map>` | Min 1 | Yes | See `pallets` |

#### shipment
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `type` | `string` | | Yes | One of `truck`, `container` |
| `license_nr` | `string` | | No | Obligatory when type is `truck` |
| `container_id` | `string` | | No | Obligatory when type is `container` |
| `from_warehouse_id` | `string` | | Yes | Where shipment is coming from | 

#### ship_to
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `residential` | `boolean` | | Yes | Residential or Commercial address |
| `name` | `string` | | Yes | Recipient |
| `att_name` | `string` | | Yes | With the attention to |
| `phone_nr` | `string` | | Yes | Phone number in the format `+COUNTRY_DIAL_CODE PHONENR` |
| `email` | `string` | | Yes | Email address |
| `address_line1` | `string` | | Yes | First address line |
| `address_line2` | `string` | | Yes | Second address line |
| `city` | `string` | | Yes | City |
| `state_province` | `string` | 2 | Yes | Example `NJ` for New Jersey |
| `postcode` | `string` | | Yes | Postcode or zipcode |
| `country_code` | `string` | 2 | Yes | Country code (e.g. `US`) |

#### ship_from
Same as `ship_to`

#### pallets
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `pallet_id` | `string` | 18 | Yes | The unique ID of the pallet |
| `weight` | `map` | | Yes | See `pallets / weight` |
| `description` | `string` | | Yes | Description of pallet content |
| `dimensions` | `map` | | Yes | See `pallets / dimensions` |
| `inventory` | `map` | | Yes | See `pallets / inventory` |

#### pallets / weight
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `unit` | `string` | Max 3 | Yes | Unit of meansurement (currently `KG`) |
| `value` | `float` | | Yes | The weight (e.g. `19.9`) |

#### pallets / dimensions
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `unit` | `string` | Max 3 | Yes | Unit of measurement (currently `CM`) |
| `width` | `float` | | Yes | The width in defined unit |
| `height` | `float` | | Yes | The height in defined unit |
| `depth` | `float` | | Yes | The depth in defined unit |

#### pallets / inventory
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `sku` | `string` | | Yes | Product SKU numbers loaded on pallet |
| `quantity` | `integer` | | Yes | Amount |
| `dimensions` | `map` | | Yes | Product SKU dimensions. See `pallets / dimensions` |
| `weight` | `map` | | Yes | Product SKU weight. See `pallets / weight` |
| `description` | `string` | | Yes | Description of SKU |
| `serial_nrs` | `array<string>` | Min 1 | Yes | All serial numbers |

