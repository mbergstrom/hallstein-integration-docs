---
title: "Delivery Instructions"
metaTitle: "EDI Delivery Instructions"
metaDescription: "This is the meta description for this page"
---

# EDI Delivery Instructions

Delivery instructions are downloadable from the EDI FTP server from the `delivery_instructions` subfolder of the warehouse.
The instructions comprised of at least 2 files. The instructions file containing the order details, together with one or more shipping labels.

```
us-warehouse-01/
  delivery_instructions/
    8add6055461843aa89d707c689f8a69c_v1.json
    8add6055461843aa89d707c689f8a69c_1Z0668E00332825788.pdf
    8add6055461843aa89d707c689f8a69c_1Z0668E00332825789.pdf
```

## Preparation and Pickup Dates
Each order contain a `prep_date` and a `pickup_date`. The `prep_date` is normally the same date as when the order EDI is sent. 
The `pickup_date` is normally the following day (depending on weekends or holidays). It is **important** that packages are 
not released to the courier before this date. If a `pickup_date` is in the past for some reason, it should be released as soon as possible.

## Warehouse opening times
The warehouse is responsible to inform Hallstein Water of their opening times and holidays when orders are not prepared or shipped out.

## Notifications
After warehouse cut-off time, an email will be sent to the warehouse containing an Excel sheet with orders that have been processed.
Sometimes an order is pushed to the warehouse **after** cut-off time. These orders will not be notified. In such cases Hallstein Water will
be in contact with the warehouse to inform of any changes that have been made.

## Versioning
Until Hallstein receives a `Loading Confirmation`, Hallstein may try to amend or cancel an order.
For these reasons, a `status` and `transaction_history_nr` is included in the instructions file.

A warehouse should always use the order instructions with the highest `transaction_history_nr`. It is a 
number that will be counted up, starting from 1.

Overall, these rules apply:

When `status` is `original`, then add the order to the job list.

When `status` is `amendment`, then delete and insert the newly received order in the job list.

When `status` is `cancelled`, then delete the order from the job list.

## EDI Message

### Filename structure for order instructions
`ORDERID_vVERSION.json`, example: `8add6055461843aa89d707c689f8a69c_v1.json`

### Filename structure for shipping labels
`ORDERID_TRACKINGNR.pdf`, example: `8add6055461843aa89d707c689f8a69c_1Z0668E00332825788.pdf`

### EDI Delivery Times
Files will be published to the FTP server during the defined opening hours set by the warehouse.

## Example 1
```json
{
  "type": "delivery-instructions",
  "transaction_history_nr": 1,
  "status": "original",
  "warehouse_id": "us-newjersey-01",
  "order_id": "8add6055461843aa89d707c689f8a69c",
  "order_date": "2020-06-30",
  "courier": {
    "id": "ups",
    "account_nr": "000000"
  },
  "ship_to": {
    "state_province": "NJ",
    "residential": true,
    "postcode": "07450",
    "phone_nr": "+1 1111111111",
    "name": "John Doe",
    "email": "john.dow@example.com",
    "country_code": "US",
    "city": "Ridgewood",
    "att_name": "",
    "address_line2": "",
    "address_line1": "123 Road"
  },
  "ship_from": {
    "state_province": "NJ",
    "residential": false,
    "postcode": "08810",
    "phone_nr": "+1 2222222222",
    "name": "Warehouse Inc.",
    "email": "hallstein@example.com",
    "country_code": "US",
    "city": "Dayton",
    "att_name": "",
    "address_line2": "Suite 102",
    "address_line1": "1 Industrial Rd"
  },
  "delivery_instructions": "Leave by gate",
  "sales_order_id": "519b039a801f4b65963638a255841931",
  "prep_date": "2020-06-30",
  "pickup_date": "2020-07-01",
  "pick_list": [
    {
      "sku": "WAT-HAL-STL-05",
      "quantity": 1
    },
    {
      "sku": "PAK-MBX-V2-HAL-WAT-STL-05",
      "quantity": 1
    }
  ],
  "packages": [
    {
      "weight": {
        "value": 19.9,
        "unit": "KG"
      },
      "tracking_nr": "1Z0668E00332825788",
      "packaging": [
        {
          "sku": "PAK-MBX-V2-HAL-WAT-STL-05",
          "quantity": 1,
          "description": "New Mailer Box for Hallstein Water 5 Gallon Bottle"
        }
      ],
      "package_id": "001",
      "label": "8add6055461843aa89d707c689f8a69c_1Z0668E00332825788.pdf",
      "inventory": [
        {
          "sku": "WAT-HAL-STL-05",
          "quantity": 1,
          "description": "Hallstein Water 5 Gallon Bottle"
        }
      ],
      "dimensions": {
        "width": 31.0,
        "unit": "CM",
        "height": 31.0,
        "depth": 57.0
      }
    }
  ]
}
```

## Example 2
```json
{
  "warehouse_id": "at-gleisdorf-01",
  "type": "delivery-instructions",
  "transaction_history_nr": 2,
  "status": "original",
  "ship_to": {
    "state_province": "",
    "residential": false,
    "postcode": "53909",
    "phone_nr": "+49 1111111111",
    "name": "John Doe",
    "email": "john.doe@example.com",
    "country_code": "DE",
    "city": "Berlin",
    "att_name": "",
    "address_line2": "",
    "address_line1": "Strasse 45"
  },
  "ship_from": {
    "state_province": "",
    "residential": false,
    "postcode": "8200",
    "phone_nr": "+43 2222222222",
    "name": "Hallstein Water Jerich AT",
    "email": "example@warehouse.com",
    "country_code": "AT",
    "city": "Gleisdorf",
    "att_name": "",
    "address_line2": "",
    "address_line1": "Mühlgasse 146"
  },
  "delivery_instructions": "",
  "sales_order_id": "1354a908029a40e8897c1b1bfe004ac3",
  "prep_date": "2020-07-28",
  "pickup_date": "2020-07-29",
  "pick_list": [
    {
      "sku": "WAT-HAL-STL-05",
      "quantity": 2
    },
    {
      "sku": "PAK-MBX-HAL-WAT-STL-05",
      "quantity": 2
    },
    {
      "sku": "DPR-OAS-ONX-CTP-BKS-EU",
      "quantity": 1
    }
  ],
  "packages": [
    {
      "weight": {
        "value": 19.8,
        "unit": "KG"
      },
      "tracking_nr": "ZCK8GBOE",
      "packaging": [
        {
          "sku": "PAK-MBX-HAL-WAT-STL-05",
          "quantity": 1,
          "description": "Mailer Box for Hallstein Water 5 Gallon Bottle"
        }
      ],
      "package_id": "001",
      "label": "a5fdd5b0bdf6425980598a44fb5850cd_ZCK8GBOE.pdf",
      "inventory": [
        {
          "sku": "WAT-HAL-STL-05",
          "quantity": 1,
          "description": "Hallstein Water 5 Gallon Bottle"
        }
      ],
      "dimensions": {
        "width": 30,
        "unit": "CM",
        "height": 30,
        "depth": 59
      }
    },
    {
      "weight": {
        "value": 19.8,
        "unit": "KG"
      },
      "tracking_nr": "ZCK8GBOF",
      "packaging": [
        {
          "sku": "PAK-MBX-HAL-WAT-STL-05",
          "quantity": 1,
          "description": "Mailer Box for Hallstein Water 5 Gallon Bottle"
        }
      ],
      "package_id": "002",
      "label": "a5fdd5b0bdf6425980598a44fb5850cd_ZCK8GBOF.pdf",
      "inventory": [
        {
          "sku": "WAT-HAL-STL-05",
          "quantity": 1,
          "description": "Hallstein Water 5 Gallon Bottle"
        }
      ],
      "dimensions": {
        "width": 30,
        "unit": "CM",
        "height": 30,
        "depth": 59
      }
    },
    {
      "weight": {
        "value": 13.6,
        "unit": "KG"
      },
      "tracking_nr": "ZCK8GBOG",
      "packaging": [],
      "package_id": "003",
      "label": "a5fdd5b0bdf6425980598a44fb5850cd_ZCK8GBOG.pdf",
      "inventory": [
        {
          "sku": "DPR-OAS-ONX-CTP-BKS-EU",
          "quantity": 1,
          "description": "Oasis Onyx Countertop Dispenser, Black/Steel EU Version"
        }
      ],
      "dimensions": {
        "width": 36,
        "unit": "CM",
        "height": 46,
        "depth": 36
      }
    }
  ],
  "order_id": "a5fdd5b0bdf6425980598a44fb5850cd",
  "order_date": "2020-07-28",
  "courier": {
    "id": "gls",
    "account_nr": "1111111111"
  }
}
```

### Elements
#### Root
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `type` | `string` | | Yes | Always `delivery-instructions` |
| `transation_history_nr` | `integer` | | Yes | The version of this message |
| `status` | `string` | | Yes | One of `original`, `amendment`, `cancellation` |
| `warehouse_id` | `string` | | Yes | The warehouse this message is intended for (e.g. `us-newjersey-01`) |
| `order_id` | `string` | 32 | Yes | The unique ID for this order |
| `order_date` | `string` | 10 | Yes | The date it was ordered (format `YYYY-MM-DD`) |
| `sales_order_id` | `string` | 32 | Yes | The sales order ID |
| `prep_date` | `string` | 10 | Yes | When it should be prepared (format `YYYY-MM-DD`) |
| `pickup_date` | `string` | 10 | Yes | When it should be release to courier (format `YYYY-MM-DD`) |
| `delivery_instructions` | `string` | | Yes | Instructions for courier to be printed on shipping labels |
| `courier` | `map` | | Yes | See `courier` |
| `ship_to` | `map` | | Yes | See `ship_to` |
| `ship_from` | `map` | | Yes | See `ship_from` |
| `pick_list` | `array<map>` | Min 1 | Yes | See `pick_list` |
| `packages` | `array<map>` | Min 1 | Yes | See `packages` |

#### courier
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `id` | `string` | | Yes | The name of the courier (e.g. `ups`) |
| `account_nr` | `string` | | Yes | The account number with the courier |
| `service` | `string` | | No | The service used with the courier (e.g. `express`) |

#### ship_to
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `residential` | `boolean` | | Yes | Residential or Commercial address |
| `name` | `string` | | Yes | Recipient |
| `att_name` | `string` | | Yes | With the attention to |
| `phone_nr` | `string` | | Yes | Phone number in the format `+COUNTRY_DIAL_CODE PHONENR` |
| `email` | `string` | | Yes | Email address |
| `address_line1` | `string` | | Yes | First address line |
| `address_line2` | `string` | | Yes | Second address line |
| `city` | `string` | | Yes | City |
| `state_province` | `string` | 2 | Yes | Example `NJ` for New Jersey |
| `postcode` | `string` | | Yes | Postcode or zipcode |
| `country_code` | `string` | 2 | Yes | Country code (e.g. `US`) |

#### ship_from
Same as `ship_to`

#### pick_list
List (or Array) of all items in this order.

| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `sku` | `string` | | Yes | SKU number (e.g. `WAT-HAL-STL-05`) |
| `quantity` | `integer` | | Yes | Amount |

#### packages
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `tracking_nr` | `string` | | Yes | Courier tracking number |
| `package_id` | `string` | 3 | Yes | The unique identifier within this order for this package (e.g. `001`, `012`) |
| `label` | `string` | | Yes | The filename of the shipping label |
| `weight` | `map` | | Yes | See `packages / weight` |
| `packaging` | `array<map>` | | Yes | See `packages / packaging` |
| `inventory` | `array<map>` | Min 1 | Yes | See `packages / inventory` |
| `dimensions` | `map` | | Yes | See `packages / dimensions` |

#### packages / weight
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `unit` | `string` | Max 3 | Yes | Unit of meansurement (currently `KG`) |
| `value` | `float` | | Yes | The weight (e.g. `19.9`) |

#### packages / packaging
A list of packaging information. Each item is a map.

| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `sku` | `string` | | Yes | The SKU number of the packaging (e.g. `PAK-MBX-V2-HAL-WAT-STL-05`) |
| `quantity` | `integer` | | Yes | Amount |
| `description` | `string` | | Yes | The description of the item | 

#### packages / inventory
A list of retail products included in this package.

| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `sku` | `string` | | Yes | The SKU number of the item (e.g. `HAL-WAT-STL-05`) |
| `quantity` | `integer` | | Yes | Amount |
| `description` | `string` | | Yes | The description of the item | 

#### packages / dimensions
| Element | Type | Length | Obligatory | Comment |
| ------- | ------ | ------ | ---------- | ------- |
| `unit` | `string` | Max 3 | Yes | Unit of measurement (currently `CM`) |
| `width` | `float` | | Yes | The width in defined unit |
| `height` | `float` | | Yes | The height in defined unit |
| `depth` | `float` | | Yes | The depth in defined unit |