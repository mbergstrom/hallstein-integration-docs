---
title: "Hallstein EDI Integration"
---
# Hallstein EDI Integration

This is brief documentation for EDI integration between Hallstein Water and Warehouse.
Documentation explains processes, EDI message types, specification of EDI messages, current product list (SKUs).
