---
title: "Products"
metaTitle: "Product list and SKU numbers"
metaDescription: "This is the meta description"
---
## Water
Please note the upcoming SKU numbers for water bottles that will come into effect 2020. These new SKU numbers will
help distinguish labels printed on the bottles as different countries have different requirements.

| SKU | EAN | Market | Title | WxHxD (cm) | Weight (kg) |
| --- | --- | ----- | ----- | ------ | ------- |
| `WAT-HAL-STL-05` | `9120090010015` | US | 5 Gallon Bottle | 25x25x45 | 19.0 |
| `WAT-HAL-STL-6PC-750` | `9120090010046` | EU/US | 6-pack of 750ml Bottles (hospitality) | 26x30x17.5 | 8.7 |
| `WAT-HAL-STL-8PC-750` | `9120090010039` | EU/US | 8-pack of 750ml Bottles | 33x35x33 | 12.7 |

### Upcoming SKUs
Changes:

`WAT-HAL-STL-05` becomes `WAT-HAL-STL-05-US`. 

`WAT-HAL-STL-6PC-750` becomes `WAT-HAL-STL-6PC-750-HTY-US`.


| SKU | EAN | Market | Title | WxHxD (cm) | Weight (kg) |
| --- | --- | ----- | ----- | ------ | ----- |
| `WAT-HAL-STL-05-US` | `9120090010015` | US | 5 Gallon Bottle | 25x25x45 | 19.0 |
| `WAT-HAL-STL-05-EU` | `9120090010091` | EU | 5 Gallon Bottle | 25x25x45 | 19.0 |
| `WAT-HAL-STL-6PC-750-HTY-US` | `9120090010046` | US | 6-pack of 750ml Bottles (hospitality) | 26x30x17.5 | 8.7 |
| `WAT-HAL-STL-6PC-750-HTY-EU` | `9120090010060` | EU | 6-pack of 750ml Bottles (hospitality) | 26x30x17.5 | 8.7 |
| `WAT-HAL-STL-6PC-750-US` | `9120090010053` | US | 6-pack of 750ml Bottles | 28x34x20 | 9.1 |
| `WAT-HAL-STL-6PC-750-EU` | `9120090010077` | EU | 6-pack of 750ml Bottles | 28x34x20 | 9.1 |
| `WAT-HAL-STL-8PC-750` | `9120090010039` | EU/US | 8-pack of 750ml Bottles | 33x35x33 | 12.7 |


### Discontinued Water SKUs
| SKU | EAN | Market | Title | WxHxD (cm) | Weight (kg) |
| --- | --- | ----- | ----- | ------ |
| `WAT-HAL-STL-6PC-700` | | EU/US | 6-pack of 700ml Bottles | 27.5x27.5x19 | 9.3 | 


## Dispensers
| SKU | Market | Title | WxHxD (cm) | Weight (kg) |
| --- | ------ | ----- | ----- | ------ |
| `DPR-HAL-HDP-WBL`         | EU/US | Hand Pump Dispenser, White / Blue                                      |   15.9x15.9x24.1 |    0.9 |
| `DPR-AQV-CLS-BCV-ALU`     | US | Aquaverve Celsius Bottle Cover, Brushed Aluminum                       |   35.6x45.7x35.6 |   2.3 |
| `DPR-AQV-CLS-BCV-BLK`     | US | Aquaverve Celsius Bottle Cover, Black                                  |   35.6x45.7x35.6 |   2.3 |
| `DPR-AQV-CLS-CTP-ALU`     | US | Aquaverve Celsius Countertop Dispenser, Brushed Aluminum               |   35.6x45.7x35.6 |   9.1 |
| `DPR-AQV-CLS-CTP-BLK`     | US | Aquaverve Celsius Countertop Dispenser, Black                          |   35.6x45.7x35.6 |   9.1 |
| `DPR-AQV-CLS-CTP-ALU-CMB` | US | Aquaverve Celsius Countertop Dispenser with Cover, Brushed Aluminum    |   35.6x45.7x35.6 |   1.1 |
| `DPR-AQV-CLS-CTP-BLK-CMB` | US | Aquaverve Celsius Countertop Dispenser with Cover, Black               |   35.6x45.7x35.6 |  11.3 |
| `DPR-AQV-CLS-STT-ALU`     | US | Aquaverve Celsius Tall Standing Dispenser, Brushed Aluminum            |   35.6x91.4x35.6 |  15.9 |
| `DPR-AQV-CLS-STT-BLK`     | US | Aquaverve Celsius Tall Standing Dispenser, Black                       |   35.6x91.4x35.6 |  15.9 |
| `DPR-AQV-CLS-STT-BLK-CMB` | US | Aquaverve Celsius Tall Standing Dispenser with Cover, Black            |   35.6x91.4x35.6 |  18.1 |
| `DPR-AQV-CLS-STT-ALU-CMB` | US | Aquaverve Celsius Tall Standing Dispenser with Cover, Brushed Aluminum |   35.6x91.4x35.6 |  18.1 |
| `DPR-OAS-ART-STT-BKS`     | US | Oasis Artesian Standing Tall Dispenser, Black/Steel                    |   35.6x106.7x35.6 |  16.8 |
| `DPR-OAS-ONX-CTP-BKS`     | US | Oasis Onyx Countertop Dispenser, Black/Steel                           |   35.6x45.7x35.6 |  13.6 |
| `DPR-OAS-ONX-CTP-BKS-UK`  | EU | Oasis Onyx Countertop Dispenser, Black/Steel UK Version                |   35.6x45.7x35.6 |  13.6 |
| `DPR-OAS-ONX-CTP-BKS-EU`  | EU | Oasis Onyx Countertop Dispenser, Black/Steel EU Version                |   35.6x45.7x35.6 |  13.6 |

## Extras
| SKU | Market | Title | WxHxD (cm) | Weight (kg) |
| --- | ------ | ----- | ----- | ------ |
| `XRA-GFT-2GL-RDL-750` | EU/US | Gift Box Set 2 Riedel Glasses with 750ml Bottle | 34x10x18.5 | 1.93

## Packaging
| SKU | Market | Title | WxHxD (cm) | Weight (kg) |
| --- | ------ | ----- | ----- | ------ |
| `PAK-BOX-HAL-WAT-STL-05`     | EU/US | Bottle Box for Hallstein Water 5 Gallon Bottle | 27x50x27 | 0.45 |
| `PAK-MBX-HAL-WAT-STL-05`     | EU/US | Old Mailer Box for Hallstein Water 5 Gallon Bottle | 29.5x29.5x59 | 0.76 |
| `PAK-MBX-INS-HAL-WAT-STL-05` | EU/US | Old Mailer Box Inserts for Hallstein Water 5 Gallon Bottle | 23x28x1 | 0.06 |
| `PAK-MBX-V2-HAL-WAT-STL-05`  | EU/US | New Mailer Box for Hallstein Water 5 Gallon Bottle | 30.5x30.5x57 | 0.9 |