---
title: "Warehouse Order Handling"
---
## Warehouse Order Handling

A simplistic example of the process flow when sending order to warehouse.

### Process
1. Order information and shipping labels are receieved via a `Delivery Instructions` EDI message.
2. Packaging material and products are picked from stock to prepare each package/parcel no sooner than on the `prep_date`.
3. Shipping labels are printed out and stuck on the intended package.
4. Serial numbers for water products are scanned and mapped to the `tracking_nr` and `package_id`.
5. Packages are handed over to the courier on the `pickup_date`.
6. A `Loading Confirmation` message is sent to the Hallstein EDI server specifying which products were used, serial numbers and quantities.
