---
title: "EDI Messages"
metaTitle: "Information about EDI messages"
metaDescription: "This is the meta description for this page"
---

#### Definitions
`Delivery Instructions` 
: An EDI message sent *to* the warehouse with instructions to prepare and ship order to customer.

`Loading Confirmation` 
: An EDI message sent *by* the warehouse when an order has been prepared and handed over to courier.

`Shipment Advice`
: An EDI message sent *to* the warehouse when an inbound shipment is about to arrive (truck / container).

`Goods Receipt` 
: An EDI message sent *by* the warehouse when inbound shipment has been received and added to inventory.

#### EDI Storage
EDI messages are uploaded to an FTP server managed by Hallstein Water.
Each warehouse has a structure as follows:
```
us-warehouse-01 /
 - delivery_instructions /
 - loading_confirmations /
 - goods_receipts /
 - shipment_advices /
```

#### Formats
All EDI messages are in `JSON` format, as such the order of elements does not matter. It is important that valid `JSON` files 
are published to the EDI server, or it will not be accepted by our system.

#### Downloading / Uploading
The warehouse is responsible to check the FTP server for new EDI messages on a regular basis.
After downloading the files, they should be deleted from the server.
**Please note** that it can happen that files are in process of being uploaded when a warehouse tries to download. 
These temporary files will be prefixed with a `~` (tilde). Those files should be ignored until their name is changed and no longer
contain the prefix.
