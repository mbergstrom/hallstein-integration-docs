const config = {
  gatsby: {
    pathPrefix: '/',
    siteUrl: 'http://integration.hallsteinwater.com',
    gaTrackingId: null,
    trailingSlash: false,
  },
  header: {
    logoLink: 'http://integration.hallsteinwater.com',
    title: 'Hallstein EDI Integration Docs',
    githubUrl: 'https://github.com/hasura/gatsby-gitbook-boilerplate',
    helpUrl: '',
    tweetText: '',
    social: '',
    links: [{ text: '', link: '' }],
    search: {
      enabled: false,
      indexName: '',
      algoliaAppId: process.env.GATSBY_ALGOLIA_APP_ID,
      algoliaSearchKey: process.env.GATSBY_ALGOLIA_SEARCH_KEY,
      algoliaAdminKey: process.env.ALGOLIA_ADMIN_KEY,
    },
  },
  sidebar: {
    forcedNavOrder: [
      '/index' // add trailing slash if enabled above
    ],
    collapsedNav: [
      '/codeblock', // add trailing slash if enabled above
    ],
    links: [{ text: 'Hallstein Water', link: 'https://www.hallsteinwater.com' }],
    frontline: false,
    ignoreIndex: true,
    title: ""
  },
  siteMetadata: {
    title: 'Integration Docs | Hallstein Water',
    description: 'Documentation for integration with Hallstein Water',
    ogImage: null,
    docsLocation: '',
    favicon: '',
  },
  pwa: {
    enabled: false, // disabling this will also remove the existing service worker.
    manifest: {
      name: 'Gatsby Gitbook Starter',
      short_name: 'GitbookStarter',
      start_url: '/',
      background_color: '#6b37bf',
      theme_color: '#6b37bf',
      display: 'standalone',
      crossOrigin: 'use-credentials',
      icons: [
        {
          src: 'src/pwa-512.png',
          sizes: `512x512`,
          type: `image/png`,
        },
      ],
    },
  },
};

module.exports = config;
