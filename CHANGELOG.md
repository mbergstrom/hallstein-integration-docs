# 0.0.5
- Added EAN number for `WAT-HAL-STL-8PC-750`.

# 0.0.4
- Updated dimensions and weights for `WAT-HAL-STL-6PC-750`, `WAT-HAL-STL-6PC-750-EU` and `WAT-HAL-STL-6PC-750-US`.

# 0.0.3
- Removed notice that `WAT-HAL-STL-8PC-750` becomes `WAT-HAL-STL-8PC-750-US`. It will not be changed.
- Added `XRA-GFT-2GL-RDL-750` gift box set.
- Added `Markets` to all products.

# 0.0.2
- Added EU SKU numbers for products.
- Fixed broken link to Hallstein website.
- Added `service` to `delivery_instructions.courier`.
- Added `delivery_instructions` to `delivery_instructions`.

# 0.0.1 
- First version published
